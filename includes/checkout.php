<?php
// Unset $_SESSION data about purchases
unset($_SESSION['purchases']);
unset($displayCategory);
unset($displayName);
unset($displayPrice);
unset($displaySize);
// Redirect to the main page
$host = $_SERVER["HTTP_HOST"];
$path = rtrim(dirname($_SERVER["PHP_SELF"]), "/\\");
header("Location: http://$host$path/index.php");
exit;
?>
