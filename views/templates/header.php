<!DOCTYPE html>

<html>
	<head>
		<link rel="stylesheet" type="text/css"  href="css/header.css" media="all"/>
		<link rel="stylesheet" type="text/css"  href="css/bootstrap.css" media="all"/>
		<link href='http://fonts.googleapis.com/css?family=Karla'  rel='stylesheet' type='text/css'>
		<title><?php echo htmlspecialchars($title) ?></title>
	</head>
	<body>
		<header>
			<h1>Three Aces Pizza</h1>
			<ul>
					<?
						$dom = simplexml_load_file("../threeaces.xml");
						foreach ($dom->xpath("/menu/items") as $items)
						{
							echo "<li>" . 
							"<a href='?page=category&n={$items["category"]}'>" . 
							$items["type"] . "</a></li>";
						}
					?>
			   </ul>
		</header>
	
