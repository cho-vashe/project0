<?
$dom = simplexml_load_file("../threeaces.xml");
//Shopping cart: prints out the purchases, enables to cancel them and check-out(without submitting)
//Get id's from POST (if there are any) and store them in SESSION['purchases']
if (isset($_POST['addToPurchases']))
{
	$_SESSION['purchases'][] = $_POST['addToPurchases'];
}
//Delete 'deleted' items
if (isset($_POST['delete']))
{
	$index = array_search($_POST['delete'], $_SESSION['purchases']);
	if($index !== FALSE)
	{
		unset($_SESSION['purchases'][$index]);
	}
}
//Display purchases if they are stored in SESSION
if (!empty($_SESSION['purchases']))
{
	echo '<div class="order-list">Your order is:<br/><table>';
	foreach ($_SESSION['purchases'] as $purchase)
		{
			foreach($dom->xpath("//*[@id='{$purchase}']") as $size);
			{
				$displaySize = $size->getName();
				foreach($size->xpath("./price") as $price);
				{
					$displayPrice = number_format(($price / 100), 2, '.', '');
					$total[] = "$price";
					foreach($price->xpath("../..") as $item);
					{
						$displayName = $item["name"];
						foreach($item->xpath("..") as $category);
						{
							$displayCategory = $category["type"];
						}
					}
				}
			}
			//print out results in the right order
			echo '<tr><td><b>' . $displayCategory . '</b></td><td>' . 
			$displayName . '</td><td>' . $displaySize . '</td><td>' . 
			$displayPrice . '</td>';
			//Show 'delete' form
			echo '<td><form action="" method="post">
			<button name="delete" class="btn btn-danger btn-mini" 
			value="' . $purchase . '"/>Delete</button></form></td></tr>';
		}
	//Display total
	echo '</table><br/><div class="total"><b>Your total: $' . 
	number_format((array_sum($total) / 100), 2, '.', '') . '</b>';
	//Display checkout form
	echo '<form method="post" action="checkout.php">
	<input type="submit" class="btn btn-info" value="Check out"></form></div>';

}	
?>
	
</div>
