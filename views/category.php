<? 
	//Loading category header
	echo '<div class="catheader">';
	$dom = simplexml_load_file("../threeaces.xml");
	foreach ($dom->xpath("/menu/items[@category='{$n}']") as $items)
	{
		echo '<h2>' . $items["type"] . '</h2></div>';
	}
	
	//print out table header with a cell for names
	echo '</br><table><thead><td></td>';
	
	//iterate through the first item and add td and title for each firstchild
	//(for example, in case 'middle' size is added)
	
	foreach ($dom->xpath("/menu/items[@category='{$n}']/item[1]/*") as $children)
	{
		echo '<td>' . $children->getName() . '</td>';
	} 
	
	//close the table header
	echo '</thead>';
	
	//start the menu table	
	foreach ($dom->xpath("/menu/items[@category='{$n}']/item") as $item)
	{
		//hardcode item's name and description fields as required
		echo '<tr><td>' . $item["name"] . '</td>';
		echo '<td>' . $item->description . '</td>';
		
		//prices are stored in cents for precision and are displayed in dd.cc format
		//button from bootstrap.css, sends id to $_POST to store in $_SESSION
		foreach ($item->xpath("*/price") as $price)
		{
		    echo '<td><form action="" method="post">
		    <button class="btn btn-success" name="addToPurchases" value=';
		    foreach ($price->xpath("../@id") as $ids)
		    {
				echo $ids;
			} 
			echo ' type="submit">
		    <i class="icon-white icon-plus"></i> $' . 
		    number_format(($price / 100), 2, '.', '') . 
		    '</button></form></td>'; 

		}
		
		echo '</tr>';
		
	}

//close the last row and the table
?>
</tr>
</table>
