<?php
// Unset $_SESSION data about purchases
session_destroy();
unset($_COOKIE[PHPSESSID]);
setcookie('PHPSESSID', '', time() - 3600);

// Redirect to the main page
$host = $_SERVER["HTTP_HOST"];
$path = rtrim(dirname($_SERVER["PHP_SELF"]), "/\\");
header("Location: http://$host$path/index.php");
exit;
?>
