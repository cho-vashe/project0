<?php
session_start();
require_once('../includes/helpers.php');



 // determine which page to render
if (isset($_GET['page']))
	$page = $_GET['page'];
else
    $page = 'index';
   
 // show page
switch ($page)
{
	case 'index':
		render('templates/header', array('title' => 'Three Aces Cafe'));
		render('index');
		render('templates/footer');
		break;

    case 'category':
		render('templates/header', array('title' => 'Three Aces Menu'));
		render('cart', array('n' => $_GET['n']));
		render('category', array('n' => $_GET['n']));
		render('templates/footer');
		break;

}

?>
